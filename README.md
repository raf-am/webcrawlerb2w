# WebCrawlerB2W

#### Desafio Técnico - Dev Backend - Web Crawler

O crawler desenvolvido tem por objetivo levantar todos os produtos ofertados no
site http://www.epocacosmeticos.com.br, registrando-os em um arquivo com
formatação **.csv** (*comma-separated values*) com o **nome do produto**, o
**título** e a **url** de cada página de produto encontrada.

Alguns parâmetros de configuração iniciais para execução do script
**"webcrawler.py"** encontram-se no arquivo em formatação JSON
**"webcrawler.config.json"**. Ambos arquivos devem compartilhar do mesmo
diretório. São suas entradas:

```
{
    "mainurl": "https://www.epocacosmeticos.com.br/",  <-- url-alvo
    "bagset": {  <---------------------------------------- configurações da sacola de produtos
        "headers": ["Nome", "Título", "URL"],  <---------- rótulos do cabeçalho .csv
        "sort": true,  <---------------------------------- produtos ordenado no .csv?
        "sortbyheader": "Nome",  <------------------------ rótulo referência para ordenação
        "fnameunique": false,  <-------------------------- nome único para o .csv (sobrescrito por execução)?
        "csvopts": {  <----------------------------------- configurações do .csv
            "sep": ",",  <-------------------------------- caracter de separação
            "index": false,  <---------------------------- exclusão de número de linha
            "mode": "w",  <------------------------------- modo de escrita
            "encoding": "utf-8",  <----------------------- mapa de caracteres
            "line_terminator": "\r\n"}  <----------------- quebra de linha
    },
    "daskset": {  <--------------------------------------- configurações do framework Dask
        "clientopts": {  <-------------------------------- configurações do client()
            "n_workers": 2,  <---------------------------- número de workers
            "threads_per_worker": 1,  <------------------- número de threads por worker
            "processes": true  <-------------------------- utilização exclusiva de processos
        }
    },
    "verbose": true  <------------------------------------ impressão de mensagens em tela
}
```

O arquivo **.csv** contendo a listagem dos produtos é salvo no mesmo diretório
dos demais arquivos mencionados. Sua nomenclatura é composta de um prefixo
não mutável *"productsbag"* e de um predicado associado ao endereço do site de
interesse (no caso, *"w<span>ww</span>.epocacosmeticos.com.br"*), ambos
conectados pelo caracter *"_"*. Se "`fnameunique: true`", um sufixo é adicionado
com um identificador único associado à data/hora de arquivamento da lista de
produtos.

> Exemplo:
> **productsbag_w<span>ww<span>.epocacosmeticos.com.br_20181204020147822231.csv**

##### --- Observações importantes ---

Alguns recursos de domínio conhecido não foram, por ora, implementados por
razões de disponibilidade de tempo e não por questões técnicas. Destacam-se,
por exemplo, estratégias para evitar bloqueio acidental por identificação de
prática abusiva de crawling/scraping tais como uso de IP/Proxy rotativo,
cabeçalho de agente baseado em usuário, entre outras.

Testes unitários
automatizados que cubram todo escopo da aplicação, absolutamente imprescindíveis
e de caráter obrigatório neste contexto, também estão pendentes de implemetação.
Seguem como testes-exemplo aqueles grosseiramente implementados junto ao arquivo
**"tests/testes_init.py"**.

A aplicação é munida de ferramental para execução tanto em **arquiteturas de
sistema centralizadas quanto distribuídas**. Isto se dá através da adoção do
framework [**Dask**](https://docs.dask.org/) -- *"a flexible library for parallel
computing in Python"*. Desta forma, é possível rodá-la desde
um cenário simplório puramente síncrono e serial, digamos num laptop doméstico
*single core*, até ambientes de computação híbrida, assíncrona e escalável de
alto desempenho, como clusters/cloudings interfaceados via Kubernetes,
Hadoop, SLURM, TORQUE, entre outros. Para tal, usa-se basicamente o mesmo
código-fonte. A documentação é vasta e alguns casos de uso podem ser encontrados
[aqui](https://docs.dask.org/en/latest/use-cases.html).

### Instalação e pré-requisitos (já testados)

* python 3.6+
* beautifulsoup 4.6+
* requests 2.20+
* dask 1.0+
* pandas 0.23+
+ ...e respectivas dependências.

Um docker está em processo de construção a partir das imagens oferecidas pela
[Continuum Analytics](https://www.anaconda.com/) no seu repositório no
[Docker Hub](https://hub.docker.com/). Em particular, é de interesse aquela que
traz a distribuição
[Miniconda3](https://hub.docker.com/r/continuumio/miniconda3/)
contendo o robusto administrador [**Conda**](https://conda.io/docs/) de pacotes
Python.

Até sua conclusão, fica como sugestão o uso do próprio **Miniconda**
([download aqui](https://conda.io/miniconda.html)) para criação do ambiente de
execução deste crawler. A documentação para criação do ambiente virtual como os
pacotes e dependências acima listados está
[aqui](https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file).
Uma vez instalado, basta executar no console o seguinte comando para criação do
ambiente:

```bash
$ conda env create -f environment.yml
```

(arquivo **"environment.yml"** disponível neste repositório)

Uma vez criado o ambiente (serão baixados e instalados todos pacotes listados
em suas respectivas versões de interesse), basta ativá-lo conforme:

* Windows: `activate webcrawlerB2W`
* MacOS & Linux: `source activate webcrawlerB2W`

Com o ambiente ativo, basta entrar no diretório onde encontra-se a aplicação e
executar:

```bash
(webcrawlerB2W)$ python webcrawler.py
```

Para os testes, basta executar:

```bash
(webcrawlerB2W)$ python -m unittest discover -s tests/
```

### Autor

Rafael A. Mattos - @raf-am - rafamattos@alumni.usp.br
