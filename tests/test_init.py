# -*- coding: utf-8 -*-
import unittest
from pandas import DataFrame
from webcrawler import init_products_bag, LOCALDIR
from os.path import exists, join


class TestInitBag(unittest.TestCase):

    def test_init_bagobject(self):
        self.assertIsInstance(init_products_bag([]), DataFrame,
                              "Should be <DataFrame>")

    def test_init_bagheaders(self):
        self.assertEqual(init_products_bag(["Nome"]).columns, ["Nome"],
                         'Should be ["Nome"]')

    def test_init_bagbadtypeheader(self):
        with self.assertRaises(TypeError):
            result = init_products_bag(1)

class TestInitConfig(unittest.TestCase):
    def setUp(self):
        self.jinitfile = join(LOCALDIR, "webcrawler.config.json")

    def test_exist_jsoninit(self):
        self.assertTrue(exists(
            self.jinitfile),
            'Should be True to "{:s}" existence'.format(self.jinitfile))

if __name__ == '__main__':
    unittest.main()
