# -*- coding: utf-8 -*-
"""
# :**B2W**: Desafio Técnico - Web Crawler

### *Vaga Dev Backend *

- **:Autor:** Rafael Augusto de Mattos
- **:E-mail:** rafamattos@alumni.usp.br
- **:Disponibilização:** 04-Dez-2018

"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

from requests import get
from bs4 import BeautifulSoup
from random import randint, shuffle
from time import sleep
from os.path import abspath, join, realpath
from urllib.parse import urlsplit
from pandas import DataFrame
from datetime import datetime as dtt
from json import loads

from dask import delayed, compute
from dask.distributed import Client

# Diretório de localização do presente script.
LOCALDIR = abspath(join(realpath(__file__), u"../"))


def load_config(jsonfname):
    """ Leitura de parâmetros de configuração. """

    # Abertura e carregamento das informações do arquivo JSON.
    with open(join(LOCALDIR, jsonfname), encoding="utf-8") as jfile:
        config = loads(jfile.read())

    return config


def init_products_bag(csvheaders):
    """ Inicialização da sacola de produtos sob listagem. """
    return DataFrame([], columns=csvheaders)


def add_products_bag(products, bag):
    """ Inclusão de produtos listados à sacola. """
    return bag.append(DataFrame(products, columns=bag.columns))


def save_products_bag(bag, **params):
    """ Exportação de produtos depositados na sacola. """

    # Remoção de produtos duplicados.
    bag.drop_duplicates(inplace=True)

    # Ordenação condicional dos produtos.
    if params["sort"]:
        bag.sort_values(params["sortbyheader"], inplace=True)

    # Exportação dos produtos para arquivo .csv. Se o parâmetro
    # "fnameunique=False", é acrescido um sufixo identificador único
    # (baseado na data/hora de execução do crawler) ao nome do arquivo.
    fnamesuffx = (""
                  if params["fnameunique"]
                  else "_{:%Y%m%d%H%M%S%f}".format(dtt.now()))

    fname = "{:s}_{:s}{:s}.csv".format("productsbag",
                                       urlsplit(params["mainurl"]).netloc,
                                       fnamesuffx)

    bag.to_csv(fname, **params["csvopts"])


def get_html_soup(url):
    """ Parser beautifulsoup sobre conteúdo HTML da página. """

    # Requisição à url.
    response = get(url)
    # Parser beautifulsoup sobre o conteúdo HTML.
    htmlsoup = BeautifulSoup(response.text, "html.parser")

    return htmlsoup


def get_nav_urls(htmlsoup):
    """ Determinação de urls da barra de navegação. """

    # Identificação da barra de navegação.
    navbar = htmlsoup.find('nav',  class_='menu')

    # levantamento dos itens de navegação (seções de produtos do site)
    # do menu principal da barra.
    allcatgs = navbar.find_all('li', class_='menu__list--item')[0]

    # Retorno das urls das seções de produtos do site.
    return [catg.get('href') for catg in
            allcatgs.find_all('a', class_='submenu__list--link')]


def get_next_url_compl(htmlsoup):
    """ Identificação de complemento das urls subsequentes. """

    # Identificação da vitrine de produtos.
    vitr = htmlsoup.find('div', class_='vitrine resultItemsWrapper')

    # Captura e retorno dos complementos das urls subsequentes contidos
    # no interior da seção em JavaScript.
    scrpt = vitr.find("script")
    scrcont = scrpt.contents[0]

    return scrcont.split(".load('")[1].split("' + pageclickednumber")[0]


def get_products_table(url):
    """ Identificação da prateleira de produtos disponíveis. """

    # Parser beautifulsoup sobre o conteúdo HTML.
    htmlsoup = get_html_soup(url)

    # Retorno dos elementos da prateleira.
    return htmlsoup, htmlsoup.find('div',
                                   class_='shelf-default prateleira')


@delayed(nout=2)
def get_products_url(url, verbose=False):
    """ Listagem de produtos por seção principal do site. """

    # Contador de página inicial (página de capa) de produtos.
    nextpagecount = 1
    products = []

    # Identificação da prateleira de produtos na página inicial.
    htmlsoup, table = get_products_table(url)

    # Varredura página-a-página das inúmeras prateleiras de produtos
    # disponíveis para determinada seção. A busca sequencial por páginas
    # (PageNumber=1, 2, 3, ...) termina quando não há mais prateleiras.
    # A cada prateleira varrida, os produtos são listados.
    while(table):
        # Listagem de produtos disponíveis na prateleira.
        productsavail = table.find_all(
            'figcaption', class_='shelf-default__image--caption')

        # Coleção de nome, título e url dos produtos listados.
        products.append([(product.find_all('a')[0].get_text(),
                          product.find_all('a')[1].get_text(),
                          product.find_all('a')[2].get('href'))
                         for product in productsavail])

        if verbose:
            print("(URL: {:s}) {:d} products @ table-#{:d} done.".format(
                url, len(productsavail), nextpagecount))

        # Contador para próxima página/prateleira.
        nextpagecount += 1

        # Pausa aleatória entre 10 e 15 segundos para próxima Varredura
        # objetivando dificultar possíveis mecanismos de identificação e
        # bloqueio de atividades de scraping e crawling no site.
        sleep(randint(10, 15))

        # # Identificação da prateleira de produtos na página seguinte.
        htmlsoup, table = get_products_table(url +
                                             get_next_url_compl(htmlsoup) +
                                             str(nextpagecount))

    # Retorno de todos produtos listados a partir de todas prateleiras
    # de uma dada seção do site e total de páginas percorridas.
    return products, nextpagecount - 1


def join_products_car(car):
    """ Concatenação de múltiplas listas de produtos em uma única. """

    # Inicialização da caixa única.
    boxlarge = []

    # Colocando todos produtos, originalmente em suas próprias
    # caixas, juntos numa única caixa dentro do carrinho.
    for boxsmall in car:
        boxlarge.extend(boxsmall)

    return boxlarge


def main():
    """ Núcleo de execução principal da aplicação. """

    # Parâmetros iniciais de configuração #
    #+++++++++++++++++++++++++++++++++++++#

    # Carregamento de parâmetros a partir de arquivo JSON local.
    inicfgparams = load_config("webcrawler.config.json")

    # URL principal visitada pelo crawler.
    mainurl = inicfgparams["mainurl"]
    # Rótulos do cabeçalho do arquivo .csv para exportação.
    csvheaders = inicfgparams["bagset"]["headers"]

    # Inicialização do Crawler #
    #++++++++++++++++++++++++++#

    # Criação do elemento master (client) da arquitetura distribuída.
    client = Client(**inicfgparams["daskset"]["clientopts"])

    # Determinação de urls secundárias a partir das seções principais de
    # produtos oferecidas pelo menu de navegação.
    htmlsoup = get_html_soup(mainurl)
    menuurls = get_nav_urls(htmlsoup)

    # Embaralhamento das urls para varredura desordenada objetivando
    # dificultar possíveis mecanismos de identificação e bloqueio de
    # atividades de scraping e crawling no site.
    shuffle(menuurls)

    # Inicialização da "carrinho" de produtos identificados.
    productscar = []

    # Inicialização da "sacola" de produtos identificados.
    productsbag = init_products_bag(csvheaders)

    # Procedimento de varredura das urls secundárias identificadas. Para
    # cada url secundária, são identificados os produtos disponíveis na
    # respectiva "página de capa" (PageNumber=1) e, de forma sequencial,
    # nas próximas páginas existentes (PageNumber=1, 2, 3, ...).

    for idxurl, menuurl in enumerate(menuurls):

        if inicfgparams["verbose"]:
            print("crawling URL ({:d}/{:d}): {:s}...".format(idxurl + 1,
                                                             len(menuurls),
                                                             menuurl))

        # Produtos identificados por url secundária (página de capa e
        # páginas subsequentes) e número total de páginas consultadas.
        menuproducts, pagesnum = get_products_url(menuurl,
                                                  inicfgparams["verbose"])

        # Agregação de produtos ao "carrinho".
        productscar.append(menuproducts)

    # Agregação das listas de produtos construídas em paralelo para
    # cada seção do site.
    productscar = compute(productscar)[0]

    # Encerramento da arquitetura distribuída.
    client.close()

    # Juntando todos produtos numa única "caixa" detro do "carrinho".
    productscarjoinedtmp = join_products_car(productscar)
    productscarjoined = join_products_car(productscarjoinedtmp)

    # Agregação de produtos à "sacola".
    productsbag = add_products_bag(productscarjoined, productsbag)

    # Exportação da "sacola" de produtos identificados no site.
    save_products_bag(productsbag,
                      mainurl=mainurl,
                      **inicfgparams["bagset"])


if __name__ == '__main__':
    main()
